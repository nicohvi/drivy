module Immutable
  def deep_dup
    Marshal.load( Marshal.dump(self) )
  end
end

class Hat
  attr_accessor :color

  def initialize (color)
    @color = color
  end
end

class Cat
  attr_accessor :name, :age, :hat

  def initialize (name, age, hat_color)
    @name = name
    @age = age
    @hat = Hat.new(hat_color) 
  end
end

class Array
  include Immutable
end

names = ["Frank", "Ted", "Nibs", "Spot", "Destructor"]
colors = ["blue", "red", "golden", "dark as night"]

cats = 5.times.map { |n|
  Cat.new(names[n], Random.new.rand(10), colors.sample)
}

litter = cats.deep_dup.map do |cat|
  cat.name = "#{cat.name} II"
  cat.age = 0
  cat.hat.color = "pink"
  cat
end

p cats.map(&:hat).map(&:color)

p litter.map(&:hat).map(&:color)
