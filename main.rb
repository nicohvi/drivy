require "json"
require 'date'

module FindableModel
  module ClassMethods
    def set_all(collection)
      @collection = collection
    end 
    
    def all
      @collection
    end 
  end

  def self.included(base)
    base.extend(ClassMethods)
  end
end

module Immutable
  def deep_dup
    Marshal.load( Marshal.dump(self) )
  end
end

module JsonSerializable 
  attr_reader :json_attrs

  def to_json(opts)
    res = {}
    @json_attrs.each { |attr| 
      res[attr] = self.instance_variable_get("@#{attr}")
    }
    res.to_json
  end

  def pretty_json(*attrs)
    @json_attrs = attrs
  end 
end



class Car
  include FindableModel

  attr_reader :id, :price_per_km, :price_per_day

  def initialize(opts)
    @id = opts['id']
    @price_per_day = opts['price_per_day']
    @price_per_km = opts['price_per_km']
  end

end

class Rental 
  include JsonSerializable
  include FindableModel
  include Immutable

  attr_accessor :id, :car, :start_date, :end_date,
    :distance, :price, :commision, :deductible, :options
    :actions

  def initialize(opts)
    @id = opts['id']
    @car = opts['car_id']
    @start_date = DateTime.parse(opts['start_date'])
    @end_date = DateTime.parse(opts['end_date'])
    @distance = opts['distance']
    @deductible = opts['deductible_reduction']
    @options = {}
    @actions = []

    pretty_json(:id, :actions)
  end

  def duration
    (@end_date - @start_date).to_i + 1
  end

  def calculate_price
    car = Car.all.detect { |car| car.id == @car }
    time_price =  apply_deduction(car.price_per_day)
    distance_price = (car.price_per_km * @distance).round
    @price = (time_price + (@distance * car.price_per_km)).round
  end

  def calculate_deductible
    @options[:deductible_reduction] = @deductible ? 
      400 * duration : 0
  end

  def calculate_commision
    @commision = Commision.new(0.3, @price, duration)
  end

  def apply_deduction(day_price)
    if duration <= 1 then return day_price * duration end
    duration.downto(1).map { |n|
      multiplier = 1
      case 
        when n > 1 && n <= 4 
          multiplier = 0.9
        when n >= 4 && n <= 10
          multiplier = 0.7
        when n > 10
          multiplier = 0.5 
      end
      multiplier * day_price
    }.inject(:+)
  end

  def generate_actions
    @actions.clear 
    @actions << Action.new('driver', 'debit', @price + @options[:deductible_reduction])
    @actions << Action.new('owner', 'credit', (@price * (1 - @commision.rate)).round)
    @actions << Action.new('insurance', 'credit', @commision.insurance_fee)
    @actions << Action.new('assistance', 'credit', @commision.assistance_fee)
    @actions << Action.new('drivy', 'credit', @commision.drivy_fee + @options[:deductible_reduction])
  end

end

class Action 
  include JsonSerializable

  attr_reader :who, :type, :amount
  
  def initialize(who, type, amount)
    @who = who
    @type = type
    @amount = amount

    pretty_json(:who, :type, :amount)
  end

 end

class Commision 
  include JsonSerializable
  attr_reader :insurance_fee, :assistance_fee, :drivy_fee, :rate

  def initialize(rate, total, days)
    @rate = rate
    gross = @rate * total
    @insurance_fee = (0.5 * gross).round
    @assistance_fee = days * 100
    @drivy_fee = (gross - @insurance_fee - @assistance_fee).round

    pretty_json(:insurance_fee, :assistance_fee, :drivy_fee)
  end
end

class RentalModification
  include JsonSerializable

  attr_reader :id, :rental_id, :actions, :start, 
    :end, :distance, :delta
  
  def initialize(opts)
    @id = opts['id']
    @rental_id = opts['rental_id']

    @rental_opts = {}
    @rental_opts[:start_date] = opts['start_date'].nil? ? nil : DateTime.parse(opts['start_date'])
    @rental_opts[:end_date] = opts['end_date'].nil? ? nil : DateTime.parse(opts['end_date'])
    @rental_opts[:distance] = opts['distance']
    @actions = []

    pretty_json(:id, :rental_id, :actions)
  end 

  def calculate
    clone = Rental.all.detect { |rental| rental.id == @rental_id }.deep_dup
  
    clone.start_date = @rental_opts[:start_date] || clone.start_date    
    clone.end_date = @rental_opts[:end_date] || clone.end_date
    clone.distance = @rental_opts[:distance] || clone.distance
    
    clone.calculate_deductible
    clone.calculate_price
    clone.calculate_commision

    @delta = calculate_delta(clone) 
  end

  def generate_actions
    @actions.clear
    @actions << Action.new('driver', @delta[:driver] > 0 ? 'debit' : 'credit', @delta[:driver].abs)
    @actions << Action.new('owner', @delta[:owner] > 0 ? 'credit' : 'debit', @delta[:owner].abs)
    @actions << Action.new('insurance', @delta[:insurance] > 0 ? 'credit' : 'debit', @delta[:insurance].abs)
    @actions << Action.new('assistance', @delta[:assistance] > 0 ? 'credit' : 'debit', @delta[:assistance].abs)
    @actions << Action.new('drivy', @delta[:drivy] > 0 ? 'credit' : 'debit', @delta[:drivy].abs)
  end

  private

  def calculate_delta(clone)
    original = Rental.all.detect { |rental| rental.id == clone.id }
    original_deduction = original.options[:deductible_reduction]
    clone_deduction = clone.options[:deductible_reduction]
    {
      driver: (clone.price + clone_deduction) - (original.price + original_deduction),
      owner:  ((clone.price*0.7) - (original.price*0.7)).round,
      insurance: clone.commision.insurance_fee - original.commision.insurance_fee,
      assistance: clone.commision.assistance_fee - original.commision.assistance_fee,
      drivy:      (clone.commision.drivy_fee + clone_deduction) - (original.commision.drivy_fee + original_deduction)
    } 
  end

end

json_file = File.read("data.json")
data = JSON.parse(json_file)

cars = data['cars']
.map{ |car| Car.new(car) }

Car.set_all(cars)

rentals = data['rentals']
.map { |rental| Rental.new(rental) }
.each { |rental| 
  rental.calculate_deductible
  rental.calculate_price 
  rental.calculate_commision
  rental.generate_actions
}

Rental.set_all(rentals)

modifications = data['rental_modifications']
.map{ |modification| RentalModification.new(modification) }
.each{ |modification| 
  modification.calculate 
  modification.generate_actions
}

File.open('output.json', 'w') do |file|
  file.write({ rental_modifications: modifications }.to_json)
end

